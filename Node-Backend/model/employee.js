const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let Employee = new Schema({
  firstName:{ type: String},

  lastName:{type: String},

  emailId:{type: String},

  dateofbirth :{type:Date},

  gender: {type:String },

  position:{type:String},
  
  salary: {type:Number},

  contactNo:{type:Number},

  city:{type:String},
  state:{type:String},
  pincode :{type:Number}
}, {
  collection: 'employees'
})
module.exports = mongoose.model('Employee', Employee)